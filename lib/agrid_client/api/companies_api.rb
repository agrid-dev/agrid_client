module AgridClient
  class CompaniesApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Return all cities attended by the company
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return [CityCompany]
    def get_cities(id, opts = {})
      data, _status_code, _headers = get_cities_with_http_info(id, opts)
      return data
    end

    #
    # Return all cities attended by the company
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(CityCompany, Fixnum, Hash)>] CityCompany data, response status code and response headers
    def get_cities_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CompaniesApi#get_cities ..."
      end

      # verify the required parameter 'id' is set
      fail "Missing the required parameter 'id' when calling get_cities" if id.nil?

      # resource path
      local_var_path = "/companies/{id}/cities".sub('{format}','json').sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<CityCompany>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CompaniesApi#get_cities\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Return all company's image
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return Array<Image>
    def get_images(id, opts = {})
      data, _status_code, _headers = get_images_with_http_info(id, opts)
      return data
    end

    #
    # Return all company's images
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(CityCompany, Fixnum, Hash)>] CityCompany data, response status code and response headers
    def get_images_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CompaniesApi#get_images ..."
      end

      # verify the required parameter 'id' is set
      fail "Missing the required parameter 'id' when calling get_images" if id.nil?

      # resource path
      local_var_path = "/companies/{id}/images".sub('{format}','json').sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<Image>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CompaniesApi#get_images\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Return a set of companies.
    # @param service_id Service ID
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :offset Number to skip rows before beginning to return companies
    # @option opts [Integer] :limit Limit number of companies on response(default is 12)
    # @return [PaginatedCompanies]
    def get_companies(service_id, opts = {})
      data, _status_code, _headers = get_companies_with_http_info(service_id, opts)
      return data
    end

    #
    # Return a set of companies.
    # @param service_id Service ID
    # @param [Hash] opts the optional parameters
    # @option opts [Integer] :offset Number to skip rows before beginning to return companies
    # @option opts [Integer] :limit Limit number of companies on response(default is 12)
    # @return [Array<(PaginatedCompanies, Fixnum, Hash)>] PaginatedCompanies data, response status code and response headers
    def get_companies_with_http_info(service_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CompaniesApi#get_companies ..."
      end

      # verify the required parameter 'service_id' is set
      fail "Missing the required parameter 'service_id' when calling get_companies" if service_id.nil?

      # resource path
      local_var_path = "/services/{service_id}/companies".sub('{format}','json').sub('{' + 'service_id' + '}', service_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'offset'] = opts[:'offset'] if opts[:'offset']
      query_params[:'limit'] = opts[:'limit'] if opts[:'limit']

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'PaginatedCompanies')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CompaniesApi#get_companies\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Return data of a specific company
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return [Company]
    def get_company(id, opts = {})
      data, _status_code, _headers = get_company_with_http_info(id, opts)
      return data
    end

    #
    # Return data of a specific company
    # @param id Company ID
    # @param [Hash] opts the optional parameters
    # @return [Array<(Company, Fixnum, Hash)>] Company data, response status code and response headers
    def get_company_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: CompaniesApi#get_company ..."
      end

      # verify the required parameter 'id' is set
      fail "Missing the required parameter 'id' when calling get_company" if id.nil?

      # resource path
      local_var_path = "/companies/{id}".sub('{format}','json').sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Company')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: CompaniesApi#get_company\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
