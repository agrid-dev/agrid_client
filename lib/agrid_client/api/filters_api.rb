module AgridClient
  class FiltersApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Returns all filters of a service.\n            You can some of the query parameters in /v1/services/:service_id/quotes\n            to fetch all filters and filter_categories of the set of that quotes.
    # @param service_id param to filter quotes by service
    # @param [Hash] opts the optional parameters
    # @option opts [String] :city_id the city id
    # @option opts [Float] :min_price the minimium price for a quote
    # @option opts [Float] :max_price the maximum price for a quote
    # @return [Filter]
    def services_service_id_filters_get(service_id, opts = {})
      data, _status_code, _headers = services_service_id_filters_get_with_http_info(service_id, opts)
      return data
    end

    #
    # Returns all filters of a service.\n            You can some of the query parameters in /v1/services/:service_id/quotes\n            to fetch all filters and filter_categories of the set of that quotes.
    # @param service_id param to filter quotes by service
    # @param [Hash] opts the optional parameters
    # @option opts [String] :city_id the city id
    # @option opts [Float] :min_price the minimium price for a quote
    # @option opts [Float] :max_price the maximum price for a quote
    # @return [Array<(Filter, Fixnum, Hash)>] Filter data, response status code and response headers
    def services_service_id_filters_get_with_http_info(service_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: FiltersApi#services_service_id_filters_get ..."
      end

      # verify the required parameter 'service_id' is set
      fail "Missing the required parameter 'service_id' when calling services_service_id_filters_get" if service_id.nil?

      # resource path
      local_var_path = "/services/{service_id}/filters".sub('{format}','json').sub('{' + 'service_id' + '}', service_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'city_id'] = opts[:'city_id'] if opts[:'city_id']
      query_params[:'min_price'] = opts[:'min_price'] if opts[:'min_price']
      query_params[:'max_price'] = opts[:'max_price'] if opts[:'max_price']

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Filter')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: FiltersApi#services_service_id_filters_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
