module AgridClient
  class LeadsApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Create a new lead
    # @param service_id Service Id
    # @param lead Lead Id
    # @param [Hash] opts the optional parameters
    # @return [Lead]
    def services_service_id_leads_post(service_id, lead, opts = {})
      data, _status_code, _headers = services_service_id_leads_post_with_http_info(service_id, lead, opts)
      return data
    end

    #
    # Create a new lead
    # @param service_id Service Id
    # @param lead Lead Id
    # @param [Hash] opts the optional parameters
    # @return [Array<(Lead, Fixnum, Hash)>] Lead data, response status code and response headers
    def services_service_id_leads_post_with_http_info(service_id, lead, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: LeadsApi#services_service_id_leads_post ..."
      end

      # verify the required parameter 'service_id' is set
      fail "Missing the required parameter 'service_id' when calling services_service_id_leads_post" if service_id.nil?

      # verify the required parameter 'lead' is set
      fail "Missing the required parameter 'lead' when calling services_service_id_leads_post" if lead.nil?

      # resource path
      local_var_path = "/services/{service_id}/leads".sub('{format}','json').sub('{' + 'service_id' + '}', service_id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = @api_client.object_to_http_body(lead)
      auth_names = []
      data, status_code, headers = @api_client.call_api(:POST, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Lead')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: LeadsApi#services_service_id_leads_post\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
