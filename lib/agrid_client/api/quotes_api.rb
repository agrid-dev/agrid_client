module AgridClient
  class QuotesApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Returns a specific quote
    # @param id Param to filter company using id
    # @param [Hash] opts the optional parameters
    # @return [Quote]
    def quotes_id_get(id, opts = {})
      data, _status_code, _headers = quotes_id_get_with_http_info(id, opts)
      return data
    end

    #
    # Returns a specific quote
    # @param id Param to filter company using id
    # @param [Hash] opts the optional parameters
    # @return [Array<(Quote, Fixnum, Hash)>] Quote data, response status code and response headers
    def quotes_id_get_with_http_info(id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: QuotesApi#quotes_id_get ..."
      end

      # verify the required parameter 'id' is set
      fail "Missing the required parameter 'id' when calling quotes_id_get" if id.nil?

      # resource path
      local_var_path = "/quotes/{id}".sub('{format}','json').sub('{' + 'id' + '}', id.to_s)

      # query parameters
      query_params = {}

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Quote')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: QuotesApi#quotes_id_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end

    #
    # Returns a set of quotes.
    # @param service_id Param to filter quotes by service
    # @param [Hash] opts the optional parameters
    # @option opts [String] :company_id Param to filter quotes by company
    # @option opts [String] :city_id Param to filter quotes by city
    # @option opts [Integer] :limit Limit number of quotes on response(default is 12)
    # @option opts [Integer] :offset Number to skip rows before beginning to return quotes
    # @option opts [Array<String>] :filter_categories Example: &lt;br&gt; filter_categories[:filter_id][]&#x3D;:filter_category_id&amp;filter_categories[:filter_id][]&#x3D;:filter_category_id &lt;br&gt;&lt;br&gt; :filter_id is the id of filter&lt;br&gt; :filter_category_id is the id of filter category
    # @option opts [Float] :min_price Param to filter quotes with price is at least equal to :min_price
    # @option opts [Float] :max_price Param to filter quotes with price is at most equal to :max_price
    # @return [PaginatedQuotes]
    def quotes_get(service_id, opts = {})
      data, _status_code, _headers = quotes_get_with_http_info(service_id, opts)
      return data
    end

    #
    # Returns a set of quotes.
    # @param service_id Param to filter quotes by service
    # @param [Hash] opts the optional parameters
    # @option opts [String] :company_id Param to filter quotes by company
    # @option opts [String] :city_id Param to filter quotes by city
    # @option opts [Integer] :limit Limit number of quotes on response(default is 12)
    # @option opts [Integer] :offset Number to skip rows before beginning to return quotes
    # @option opts [Array<String>] :filter_categories Example: &lt;br&gt; filter_categories[:filter_id][]&#x3D;:filter_category_id&amp;filter_categories[:filter_id][]&#x3D;:filter_category_id &lt;br&gt;&lt;br&gt; :filter_id is the id of filter&lt;br&gt; :filter_category_id is the id of filter category
    # @option opts [Float] :min_price Param to filter quotes with price is at least equal to :min_price
    # @option opts [Float] :max_price Param to filter quotes with price is at most equal to :max_price
    # @return [Array<(PaginatedQuotes, Fixnum, Hash)>] PaginatedQuotes data, response status code and response headers
    def quotes_get_with_http_info(service_id, opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: QuotesApi#quotes_get ..."
      end

      # verify the required parameter 'service_id' is set
      fail "Missing the required parameter 'service_id' when calling quotes_get" if service_id.nil?

      # resource path
      local_var_path = "/services/{service_id}/quotes".sub('{format}','json').sub('{' + 'service_id' + '}', service_id.to_s)

      # query parameters
      query_params = {}
      query_params[:'company_id'] = opts[:'company_id'] if opts[:'company_id']
      query_params[:'city_id'] = opts[:'city_id'] if opts[:'city_id']
      query_params[:'limit'] = opts[:'limit'] if opts[:'limit']
      query_params[:'offset'] = opts[:'offset'] if opts[:'offset']
      query_params[:'filter_categories'] = @api_client.build_collection_param(opts[:'filter_categories'], :csv) if opts[:'filter_categories']
      query_params[:'min_price'] = opts[:'min_price'] if opts[:'min_price']
      query_params[:'max_price'] = opts[:'max_price'] if opts[:'max_price']

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'PaginatedQuotes')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: QuotesApi#quotes_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
