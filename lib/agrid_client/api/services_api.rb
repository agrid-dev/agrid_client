module AgridClient
  class ServicesApi
    attr_accessor :api_client

    def initialize(api_client = ApiClient.default)
      @api_client = api_client
    end

    #
    # Returns all available services
    # @param [Hash] opts the optional parameters
    # @option opts [String] :city_id City ID to filter services by city
    # @option opts [String] :tag Tag to filter a subset of services
    # @return [Array<Service>]
    def services_get(opts = {})
      data, _status_code, _headers = services_get_with_http_info(opts)
      return data
    end

    #
    # Returns all available services
    # @param [Hash] opts the optional parameters
    # @option opts [String] :city_id City ID to filter services by city
    # @option opts [String] :tag Tag to filter a subset of services
    # @return [Array<(Array<Service>, Fixnum, Hash)>] Array<Service> data, response status code and response headers
    def services_get_with_http_info(opts = {})
      if @api_client.config.debugging
        @api_client.config.logger.debug "Calling API: ServicesApi#services_get ..."
      end

      # resource path
      local_var_path = "/services".sub('{format}','json')

      # query parameters
      query_params = {}
      query_params[:'city_id'] = opts[:'city_id'] if opts[:'city_id']
      query_params[:'tag'] = opts[:'tag'] if opts[:'tag']

      # header parameters
      header_params = {}

      # HTTP header 'Accept' (if needed)
      local_header_accept = ['application/json']
      local_header_accept_result = @api_client.select_header_accept(local_header_accept) and header_params['Accept'] = local_header_accept_result

      # HTTP header 'Content-Type'
      local_header_content_type = ['application/json']
      header_params['Content-Type'] = @api_client.select_header_content_type(local_header_content_type)

      # form parameters
      form_params = {}

      # http body (model)
      post_body = nil
            auth_names = []
      data, status_code, headers = @api_client.call_api(:GET, local_var_path,
        :header_params => header_params,
        :query_params => query_params,
        :form_params => form_params,
        :body => post_body,
        :auth_names => auth_names,
        :return_type => 'Array<Service>')
      if @api_client.config.debugging
        @api_client.config.logger.debug "API called: ServicesApi#services_get\nData: #{data.inspect}\nStatus code: #{status_code}\nHeaders: #{headers}"
      end
      return data, status_code, headers
    end
  end
end
