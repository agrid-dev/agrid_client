require 'date'
require_relative 'concerns/swagger_model'

module AgridClient
  class Sale
    include SwaggerModel

    attr_accessor :price, :expiration_date, :note

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'price' => :'price',
        :'expiration_date' => :'expiration_date',
        :'note' => :'note',
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'price' => :'Float',
        :'expiration_date' => :'DateTime',
        :'note' => :'String',
      }
    end
  end
end
