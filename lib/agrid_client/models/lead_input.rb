=begin
Agrid Quotes API



OpenAPI spec version: 1.0

Generated by: https://github.com/swagger-api/swagger-codegen.git


=end

require 'date'
require_relative 'concerns/swagger_model'

module AgridClient
  class LeadInput
    include SwaggerModel

    attr_accessor :message

    # The id which will identify your leads
    attr_accessor :partner_id

    attr_accessor :quote_id

    attr_accessor :company_id

    attr_accessor :customer

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'message' => :'message',
        :'partner_id' => :'partner_id',
        :'quote_id' => :'quote_id',
        :'company_id' => :'company_id',
        :'customer' => :'customer'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'message' => :'String',
        :'partner_id' => :'String',
        :'quote_id' => :'String',
        :'company_id' => :'String',
        :'customer' => :'CustomerInput'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      if attributes[:'message']
        self.message = attributes[:'message']
      end
      if attributes[:'partner_id']
        self.partner_id = attributes[:'partner_id']
      end
      if attributes[:'quote_id']
        self.quote_id = attributes[:'quote_id']
      end
      if attributes[:'company_id']
        self.company_id = attributes[:'company_id']
      end
      if attributes[:'customer']
        self.customer = attributes[:'customer']
      end
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          message == o.message &&
          partner_id == o.partner_id &&
          quote_id == o.quote_id &&
          company_id == o.company_id &&
          customer == o.customer
    end
  end
end
