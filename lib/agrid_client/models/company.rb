require 'date'
require_relative 'concerns/swagger_model'

module AgridClient
  class Company
    include SwaggerModel

    # Unique id
    attr_accessor :id, :email, :name, :person_in_charge, :website, :location,
      :addresses, :phones, :logo, :cover, :data_checks, :rankings, :rating,
      :description, :comission_rate

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'id' => :'id',
        :'email' => :'email',
        :'name' => :'name',
        :'person_in_charge' => :'person_in_charge',
        :'website' => :'website',
        :'location' => :'location',
        :'addresses' => :'addresses',
        :'phones' => :'phones',
        :'logo' => :'logo',
        :'cover' => :'cover',
        :'data_checks' => :'data_checks',
        :'rankings' => :'company_rankings',
        :'rating' => :'rating',
        :'description' => :'description',
        :'comission_rate' => :'comission_rate'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'id' => :'String',
        :'email' => :'String',
        :'name' => :'String',
        :'person_in_charge' => :'String',
        :'website' => :'String',
        :'location' => :'Location',
        :'addresses' => :'Array<Address>',
        :'phones' => :'Array<Phone>',
        :'logo' => :'String',
        :'cover' => :'String',
        :'data_checks' => :'Array<DataCheck>',
        :'rankings' => :'Array<CompanyRanking>',
        :'rating' => 'Float',
        :'description' => :'String',
        :'comission_rate' => :'Float'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      if attributes[:'id']
        self.id = attributes[:'id']
      end
      if attributes[:'email']
        self.email = attributes[:'email']
      end
      if attributes[:'name']
        self.name = attributes[:'name']
      end
      if attributes[:'person_in_charge']
        self.person_in_charge = attributes[:'person_in_charge']
      end
      if attributes[:'website']
        self.website = attributes[:'website']
      end
      if attributes[:'location']
        self.location = attributes[:'location']
      end
      if attributes[:'addresses']
        self.addresses = attributes[:'addresses']
      end
      if attributes[:'phones']
        self.phones = attributes[:'phones']
      end
      if attributes[:'data_checks']
        self.data_checks = attributes[:'data_checks']
      end
      if attributes[:'rankings']
        self.rankings = attributes[:'rankings']
      end
      if attributes[:'description']
        self.description = attributes[:'description']
      end
      if attributes[:'comission_rate']
        self.comission_rate = attributes[:'comission_rate']
      end
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          id == o.id
    end
  end
end
