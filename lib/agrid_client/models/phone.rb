=begin
Agrid Quotes API



OpenAPI spec version: 1.0

Generated by: https://github.com/swagger-api/swagger-codegen.git


=end

require 'date'
require_relative 'concerns/swagger_model'

module AgridClient
  class Phone
    include SwaggerModel

    # Company phone number
    attr_accessor :number, :person_in_charge, :phone_type

    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'number' => :'number',
        :'person_in_charge' => :'person_in_charge',
        :'phone_type' => :'phone_type'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'number' => :'String',
        :'person_in_charge' => :'String',
        :'phone_type' => :'String'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      
      self.number           = attributes[:'number'] if attributes[:'number']
      self.person_in_charge = attributes[:'person_in_charge'] if attributes[:'person_in_charge']
      self.phone_type       = attributes[:'phone_type'] if attributes[:'phone_type']
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          number == o.number &&
          person_in_charge == o.person_in_charge &&
          phone_type == o.phone_type
    end
  end
end
