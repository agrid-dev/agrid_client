require_relative 'concerns/swagger_model'

module AgridClient
  class DataCheck
    include SwaggerModel

    attr_accessor :type, :status

    def self.attribute_map
      {
        :'type' => :'type',
        :'status' => :'status'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'type' => :'String',
        :'status' => 'String'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      self.type = attributes[:'type'] if attributes[:'type']
      self.status = attributes[:'status'] if attributes[:'status']
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          type == o.type &&
          status == o.status
    end
  end
end
