require_relative 'concerns/swagger_model'

module AgridClient
  class CompanyRanking
    include SwaggerModel

    attr_accessor :service, :rank

    def self.attribute_map
      {
        :'service' => :'service',
        :'rank' => :'rank'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'service' => :'Service',
        :'rank' => :'Float'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      self.service = attributes[:'service'] if attributes[:'service']
      self.ranking = attributes[:'ranking'] if attributes[:'ranking']
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          service == o.service &&
          ranking == o.ranking
    end
  end
end
