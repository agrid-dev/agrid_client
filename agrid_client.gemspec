# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "agrid_client/version"

Gem::Specification.new do |s|
  s.name        = "agrid-client"
  s.version     = AgridClient::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Guilherme B. Moretti"]
  s.email       = ["guilhermebmoretti@gmail.com"]
  s.homepage    = "http://agrid.com.br"
  s.summary     = "Ruby client for Agrid's prices API"
  s.description = ""
  s.license     = "MIT"


  s.add_runtime_dependency 'typhoeus', '~> 1.0'
  s.add_runtime_dependency 'json', '~> 1.8'

  s.add_development_dependency 'rspec', '~> 3.4'

  s.files         = `find *`.split("\n").uniq.sort.select{ |f| !f.empty? && !(f.to_s =~ /^.*\.gem$/) }
  s.test_files    = `find spec/*`.split("\n")
  s.executables   = []
  s.require_paths = ["lib"]
end
